import { defineStore } from "pinia"
import { computed, reactive, ref } from "vue"

// 定义store
/* 
  store的名字
    useXxxStore
    student -> useStudentStore
    cart -> useCartStore
  defineStore()
    - 参数：
        id store的唯一表示，不能重复
        {} store配置
    - 返回值：函数
   
  概念：
    pinia实例 createPinia()
    useXxx函数 defineStore()
    store实例（响应式对象） use函数

  defineStore的配置对象：
    选项式API（推荐）
      - 配置对象
      - 选项：
          state
            - state用来配置store中存储的数据
            - 这些数据可以直接通过store实例访问
            - 需要一个函数作为值，函数需要返回一个对象，
                对象中的属性就会成为store实例中存储的状态

          getters
            - getters就相当于Vue中的计算属性
            - 需要一个对象作为值，对象中通过方法来设置计算属性，
                方法中可以通过this来访问当前的store实例
            - 计算属性也可以直接通过store实例来访问

          actions
            - actions相当于组件中定义的方法，可以在方法中定义一些操作的逻辑
            - actions中的定义的方法，可以通过store实例直接调用
            - 它也需要一个对象做为值，对象中的方法就是最终的函数
                在方法中可以通过this来访问store实例
    组合式API

*/
export const useStudentStore = defineStore("student", {
  state: () => ({
    name: "孙悟空",
    age: 18,
    gender: "男",
    weapons: ["金箍棒", "三根毛"],
  }),
  getters: {
    doubleAge() {
      return this.age * 2
    },
    titleName: (state) => "Mr." + state.name,
  },
  actions: {
    increase() {
      console.log(11111)

      this.age++

      return "哈哈哈"
    },

    setAge(age) {
      this.age = age
    },
  },
})

// //组合式API
// export const useStudentStore = defineStore("student", () => {
//   // 定义state
//   const name = ref("猪八戒")
//   const age = ref(28)
//   const gender = ref("男")

//   // 计算属性 getters
//   const doubleAge = computed(() => age.value * 2)
//   const titleName = computed(() => "Mr." + name.value)

//   // actions
//   const increase = () => {
//     age.value++
//   }

//   const setAge = (val) => {
//     age.value = val
//   }

//   return { name, age, gender, doubleAge, titleName, increase, setAge }
// })

export const useHelloStore = defineStore("hello", {})

export const useWorldStore = defineStore("world", {})
