const obj = {
  name: "孙悟空",
  age: 18,
  gender: "男",
}

const objProxy = new Proxy(obj, {
  get(target, propName) {
    // 做一些其他操作，记录谁使用了该值
    console.log("xxxx访问了" + propName)
    return target[propName]
  },

  set(target, propName, value) {
    console.log(propName + "发生变化了，所有使用该属性的组件重新渲染")
    target[propName] = value
  },
})

// 对代理对象所做的操作，最终都会在原对象上体现出来
obj.age = 29
console.log(obj.age)
