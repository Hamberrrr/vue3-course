# vue3-course

## Hello World

### Vue

1. 前端框架
2. 渐进式
3. 声明式
4. 组件化

### 前置知识

1. HTML&CSS
2. JavaScript基础知识（ES6后的知识）
3. Node.js
4. Vite/Webpack（可选）
5. git（可选）
6. TypeScript（可选）

### 环境

node.js 16+

### 代码

1. 创建项目 `npm create vue`
1. 根据提示填写信息
1. 进入项目目录，输入`npm install`安装依赖
1. 使用`npm run dev`来启动开发模式
1. 访问项目路径

### 手写

`main.js`主文件、入口文件

```javascript
import { createApp } from "vue"
import App from "@/App.vue"

// 创建应用实例
/* 
  createApp()
    - 用来创建一个应用实例
    - 参数：
        需要一个根组件作为参数
*/
const app = createApp(App)

// 将应用实例挂载到页面中
app.mount("#app")
```

`App.vue`根组件

```vue
<template>
  <h1>你好，Vue</h1>
</template>
```

### 组件

组件是构成一个项目的基本单位，一个项目就是有多个组件混合而成的。编写项目实际上就是在定义组件！

在Vue中我们使用的是单文件组件，组件中的所有代码（包括结构、表现、行为）统一写在一个`.vue`文件中。一个Vue文件可以包含三个部分：

1. 结构：`<template></template>`
2. 表现：`<style></style>`
3. 行为：`<script></script>`

### API风格

1. 选项式API

   ```vue
   <script>
   export default {
     // data() 返回的属性将会成为响应式的状态
     // 并且暴露在 `this` 上
     data() {
       return {
         count: 0
       }
     },
   
     // methods 是一些用来更改状态与触发更新的函数
     // 它们可以在模板中作为事件处理器绑定
     methods: {
       increment() {
         this.count++
       }
     },
   
     // 生命周期钩子会在组件生命周期的各个不同阶段被调用
     // 例如这个函数就会在组件挂载完成后被调用
     mounted() {
       console.log(`The initial count is ${this.count}.`)
     }
   }
   </script>
   
   <template>
     <button @click="increment">Count is: {{ count }}</button>
   </template>
   ```

2. 组合式API

   ```vue
   <script setup>
   import { ref, onMounted } from 'vue'
   
   // 响应式状态
   const count = ref(0)
   
   // 用来修改状态、触发更新的函数
   function increment() {
     count.value++
   }
   
   // 生命周期钩子
   onMounted(() => {
     console.log(`The initial count is ${count.value}.`)
   })
   </script>
   
   <template>
     <button @click="increment">Count is: {{ count }}</button>
   </template>
   ```


### 组件的本质

组件本质上就是一个JS的模块！组件中的所有代码最终都会转换为JS。之所以将template和style都转换为js代码就是为了方便我们动态的去显示页面，简单说就是方便向其中添加变量。

在template中，我们是以html的形式在编写js的代码。在style中，我们是以css的形式编写js的代码！

单文件组件（.vue）----编译----> JS代码，由编译器负责！

### 虚拟DOM

组件并没有被直接转换DOM代码，因为DOM操作是比较耗费性能的，非常容易触发浏览器的重绘重排。如果直接将组件转换为DOM代码，大概率不会有太好的性能。

为了解决这个问题，可以将所有的需要操作DOM的代码给它汇总起来，然后进行统一处理，这样就可以避免重复的DOM操作，提高浏览器的渲染性能。

用普通的JS对象来描述真实的DOM结构 —— 虚拟DOM

虚拟DOM ---渲染器---> 真实DOM

单文件组件（.vue）----编译器----> 虚拟DOM ---渲染器---> 真实DOM

## 基础知识

### 响应式原理

响应式对象，当响应式对象发生变化时，与之绑定的界面也会随之属性。

当我们调用`reactive()`时，需要一个普通的JS对象作为参数，它会给我们返回一个代理对象作为返回值。通过代理对象可以对原本的对象进行各种操作，但是在操作的同时代理对象可以实现更多的功能！代理对象主要做了这么两件事：

1. 当有组件使用响应式属性时，代理对象会对其进行记录。
2. 当响应式属性发生变化时，代理对象会通知所有使用该属性的组件进行重新渲染。
